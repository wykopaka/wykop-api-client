#### 1.1.1 (2021-10-08)

  * Fixed problem with entries creation.

#### 1.1.0 (2020-11-30)

  * Adds support for Tags listing.

## 1.0.0 (2019-08-11)

  * Initial release
