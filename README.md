# Wykop API Client 

This library provides you a client for a Wykop API v3.

### Requirements

- Wykop API Client  works with PHP 5.5 or above.

### Installation

Install the latest version with

```bash
composer require wykopaka/wykop-api-client:v2.x-dev
```


## Documentation

### Basic Usage
```php
<?php

use WykopApiClient\WykopApi;

$apiClient = new WykopApi();
$apiClient->authorizeApp('APPKEY', 'APPSECRET');
```
### Wykop Connect
#### Get Wykop Connect URL
```php
$redirectUrl = $apiClient->wykopConnect->getUrl();
header('Location: ' . $redirectUrl);
```

### Tags
#### Get entries from a tag.
```php
$entries = $apiClient->tags->getEntries(
    'wykopaka' // Tag name.
);

foreach ($entries as $entry) {
    // ...
}
```

### Entries
#### Post a new entry.
```php
$entries = $apiClient->entries->post(
    'Message content', // Message contentn.
    'https://example.com/image.png', // Attachment URL.
    false // True if it is an adult only content.
);
```

#### Get user's entries.
```php
$entries = $apiClient->profile->getEntries(
    'wykopaka' // Username.
);

foreach ($entries as $entry) {
    // ...
}
```

### Profiles
#### Get user's profile.
```php
$entries = $apiClient->profile->get(
    'wykopaka' // Username.
);


```