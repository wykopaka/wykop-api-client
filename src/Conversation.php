<?php

namespace WykopApiClient;

use stdClass;

class Conversation
{

    private $data = null;

    public function __construct($entry) {
        $this->data = $entry;
    }

    public function getUser()
    {
        return new Profile($this->data->user);
    }

    public function getLastMessage()
    {
        return new PrivateMessage($this->data->last_message);
    }

    public function getUnread()
    {
        return $this->data->unread;
    }

}
