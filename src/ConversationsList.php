<?php

namespace WykopApiClient;

class ConversationsList extends EntitiesList
{

    public function append(mixed $value) : void
    {
        parent::append(new Conversation($value));
    }

}
