<?php

namespace WykopApiClient;

use ArrayObject;
use stdClass;

class EntitiesList extends ArrayObject
{

    /**
     * @var array Array of values returned by Wykop API.
     * @deprecated deprecated since version 2.0.
     */
    public $data = [];

    /**
     * @var null|EntitiesPagination Pagination object.
     */
    public $pagination = null;

    /**
     * Construct a new array object
     * @link https://php.net/manual/en/arrayobject.construct.php
     * @param array|object $array The input parameter accepts an array or an Object.
     * @param int $flags Flags to control the behaviour of the ArrayObject object.
     * @param string $iteratorClass Specify the class that will be used for iteration of the ArrayObject object. ArrayIterator is the default class used.
     */
    public function __construct(object|array $entities = [], int $flags = 0, string $iteratorClass = 'ArrayIterator')
    {
        $this->data = $entities;

        foreach ($entities as $entity) {
            $this->append($entity);
        }

        return $this;
    }

    /**
     * Sets EntitiesPagination object as a pagination property value.
     *
     * @param stdClass $pagination
     * @param mixed|null $currentPage
     * @return void
     */
    public function setPagination(stdClass $pagination, mixed $currentPage = null) : void
    {
        $this->pagination = new EntitiesPagination($pagination, $currentPage);
    }

    /**
     * Returns pagination property value.
     *
     * @return EntitiesPagination
     */
    public function getPagination() : EntitiesPagination
    {
        return $this->pagination;
    }

    /**
     * Getter method.
     *
     * @param string $property
     * @return mixed
     */
    public function __get(string $property) : mixed
    {
        if ('data' === $property) {
            trigger_error(
                'Property EntriesList::$data is deprecated and should no longer be used.',
                E_USER_DEPRECATED
            );
        }
    }

    /**
     * Append value into the ArrayObject.
     *
     * @param mixed $value
     * @return void
     */
//    public function append(mixed $value) : void
//    {
//        parent::append(new Entry($value));
//    }

}
