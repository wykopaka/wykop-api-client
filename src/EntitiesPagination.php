<?php

namespace WykopApiClient;

use ArrayObject;

class EntitiesPagination {

    /**
     * @var int|string|null Next page number or hash.
     */
    private $nextPage = null;

    /**
     * @var int|string|null Previous page number or hash.
     */
    private $prevPage = null;

    /**
     * EntitiesPagination constructor.
     *
     * @param object $pagination Pagination object.
     * @param int|string|null $currentPage Current page number or hash.
     */
    public function __construct(object $pagination, int|string|null $currentPage = null)
    {
        $this->setPrevPage($pagination, $currentPage);
        $this->setNextPage($pagination, $currentPage);
    }

    /**
     * Calculates and sets previous page number or hash.
     *
     * @param object $pagination Pagination object.
     * @param int|string|null $currentPage Current page number or hash.
     * @return void
     */
    private function setPrevPage(object $pagination, int|string|null $currentPage = null) : void
    {
        if ($pagination && property_exists($pagination, 'prev')) {
            $this->prevPage = $pagination->prev;
        } else {
            $currentPage = $currentPage ?? 1;

            $this->prevPage = ($currentPage > 1) ? ($currentPage - 1) : null;
        }
    }

    /**
     * Calculates and sets next page number or hash.
     *
     * @param object $pagination Pagination object.
     * @param int|string|null $currentPage Current page number or hash.
     * @return void
     */
    private function setNextPage(object $pagination, int|string|null $currentPage = null) : void
    {
        if ($pagination && property_exists($pagination, 'next')) {
            $this->nextPage = $pagination->next;
        } else {
            $currentPage = $currentPage ?? 1;

            $this->nextPage = ($pagination
                && property_exists($pagination, 'per_page')
                && property_exists($pagination, 'total')
                && ($currentPage * $pagination->per_page <= $pagination->total)) ? ($currentPage + 1) : null;
        }
    }

}
