<?php

namespace WykopApiClient;

use stdClass;

class Entries
{
    /**
     * @var WykopApi $apiClient
     */
    private $apiClient = null;

    /**
     * Entries constructor.
     *
     * @param WykopApi  $apiClient
     */
    public function __construct($apiClient) {
        $this->apiClient = $apiClient;
    }

    /**
     * Posts a new entry.
     *
     * @param string $body          A message's content.
     * @param null   $embed         Attached image/video url address.
     * @param bool   $adultMedia    Embed media contains a content for adults.
     *
     * @return stdClass
     *
     * @throws Error
     */
    public function post($body, $embed = null, $adultMedia = false) {
        if (empty($body)) {
            throw new WykopApiError('Body parameter for Entries::post() is required!');
        }

        if (!empty($embed) && !filter_var($embed, FILTER_VALIDATE_URL)) {
            throw new WykopApiError('Embed parameter for Entries::post() should be an URL address!');
        }

        if (!is_bool($adultMedia)) {
            throw new WykopApiError('Adult media parameter for Entries::post() should be a boolean value!');
        }

        $postData = [];
        $postData['data'] = [
            'content' => $body,
            'adult' => $adultMedia
        ];

        if ($embed) {
            $atachment = $this->apiClient->request('media/photos?type=comments', json_encode([
                'data' => [
                    'url' => $embed
                ]
            ]));

            $postData['data']['photo'] = $atachment->data->key;
        }

        $entry = $this->apiClient->request('entries', json_encode($postData));

        return new Entry($entry->data);
    }
}
