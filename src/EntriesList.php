<?php

namespace WykopApiClient;

class EntriesList extends EntitiesList
{

    public function append(mixed $value) : void
    {
        parent::append(new Entry($value));
    }

}
