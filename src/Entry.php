<?php

namespace WykopApiClient;

use stdClass;

class Entry
{
    private $data = null;

    public function __construct($entry) {
        $this->data = $entry;
    }

    public function getId()
    {
        return $this->data->id;
    }

    public function getSlug()
    {
        return $this->data->slug;
    }

    public function getAuthor()
    {
        return new Profile($this->data->author);
    }

    public function getCreatedAt()
    {
        return date_create_from_format('Y-m-d H:i:s', $this->data->created_at);
    }

    public function getContent() : string
    {
        return $this->data->content;
    }

    public function getPicture()
    {
        return $this->data->media->photo
            ? new Media($this->data->media->photo)
            : null;
    }

    public function getEmbed()
    {
        return $this->data->media->embed
            ? new Media($this->data->media->embed)
            : null;
    }

    public function getAdult() : bool
    {
        return $this->data->adult;
    }

    public function getTags() : array
    {
        return $this->data->tags;
    }

    public function getComments()
    {
        return $this->data->comments; // TODO: to powinna być klasa
    }

    public function getVotes()
    {
        $votesObject = new stdClass();
        $votesObject->up = $this->data->votes->up;
        $votesObject->down = $this->data->votes->down;
        return $votesObject;
    }

    public function getResource()
    {
        return $this->data->resource; // TODO: to powinna być klasa
    }
}
