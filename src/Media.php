<?php

namespace WykopApiClient;

use stdClass;

class Media
{

    public $data = null;

    public function __construct($entry) {
        $this->data = $entry;
    }

    public function getKey()
    {
        return $this->data->key;
    }

    public function getType()
    {
        return $this->data->mime_type ?? $this->data->type ?? null;
    }

    public function getUrl()
    {
        return $this->data->url;
    }

    public function getThumbnail()
    {
        return $this->data->thumbnail ?? null;
    }

    public function getLabel()
    {
        return $this->data->label ?? null;
    }

    public function getSize()
    {
        return $this->data->size ?? null;
    }

    public function getResolution()
    {
        return $this->data->width ? [$this->data->width, $this->data->height] : null;
    }

}
