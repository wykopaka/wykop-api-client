<?php

namespace WykopApiClient;

use stdClass;

class PrivateMessage
{

    public $data = null;

    public function __construct($entry) {
        $this->data = $entry;
    }

    public function getAdult()
    {
        return $this->data->adult;
    }

    public function getContent()
    {
        return $this->data->content;
    }

    public function getType()
    {
        return $this->data->type;
    }

    public function getCreatedAt()
    {
        return date_create_from_format('Y-m-d H:i:s', $this->data->created_at);
    }

    public function getRead()
    {
        return $this->data->read;
    }

    public function getKey()
    {
        return $this->data->key;
    }

    public function getPicture()
    {
        return $this->data->media->photo
            ? new Media($this->data->media->photo)
            : null;
    }

    public function getEmbed()
    {
        return $this->data->media->embed
            ? new Media($this->data->media->embed)
            : null;
    }

}
