<?php

namespace WykopApiClient;

use stdClass;

class PrivateMessages
{
    /**
     * @var WykopApi $apiClient
     */
    private $apiClient = null;

    public function __construct($apiClient) {
        $this->apiClient = $apiClient;
    }

    public function getConversations() {
        $conversations = $this->apiClient->request(
            'pm/conversations'
        );

        $conversationsList = new ConversationsList($conversations->data);

        return $conversationsList;
    }

    public function getConversation($receiver, $nextMessage = null) {
        if (empty($receiver)) {
            throw new WykopApiError('Receiver parameter for PrivateMessage::getConversation() is required!');
        }

        $privateMessages = $this->apiClient->request(
            'pm/conversations/' . $receiver . ($nextMessage ? '?next_message=g0zKk2AR' : '')
        );

        $privateMessagesList = new PrivateMessagesList($privateMessages->data->messages);
        $privateMessagesList->setUser($privateMessages->data->user);

        return $privateMessagesList;
    }

    public function sendMessage($receiver, $body, $embed = null)
    {
        if (empty($receiver)) {
            throw new WykopApiError('Receiver parameter for PrivateMessage::sendMessage() is required!');
        }

        if (empty($body)) {
            throw new WykopApiError('Body parameter for PrivateMessage::sendMessage() is required!');
        }

        $payload = [];
        $payload['data']['content'] = $body;

        if ($embed) {
            $atachment = $this->apiClient->request('media/photos?type=comments', json_encode([
                'data' => [
                    'url' => $embed
                ]
            ]));

            $payload['data']['photo'] = $atachment->data->key;
        }

        $response = $this->apiClient->request('pm/conversations/' . $receiver, json_encode($payload));

        return new PrivateMessage($response->data);
    }

}
