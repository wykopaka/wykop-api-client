<?php

namespace WykopApiClient;

class PrivateMessagesList extends EntitiesList
{

    public $user = null;

    public function append(mixed $value) : void
    {
        parent::append(new PrivateMessage($value));
    }

    public function setUser($user) : void
    {
        $this->user = new Profile($user);
    }

    public function getUser()
    {
        return $this->user;
    }

}
