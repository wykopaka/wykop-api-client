<?php

namespace WykopApiClient;

use stdClass;

class Profile
{
    /** @deprecated  */
    public $data = null;

    public function __construct($entry) {
        $this->data = $entry;
    }

    public function getUsername()
    {
        return $this->data->username;
    }

    public function getGender()
    {
        return $this->data->gender;
    }

    public function getCompany()
    {
        return $this->data->company;
    }

    public function getAvatar()
    {
        return $this->data->avatar;
    }

    public function getStatus()
    {
        return $this->data->status;
    }

    public function getColor()
    {
        return $this->data->color; // @todo Retrieve color from API.
    }

    public function getSummary()
    {
        $summary = new stdClass();
        $summary->actions = $this->data->summary->actions;
        $summary->links = $this->data->summary->links;
        $summary->entries = $this->data->summary->entries;
        $summary->followers = $this->data->summary->followers;
        $summary->following_users = $this->data->summary->following_users;
        $summary->following_tags = $this->data->summary->following_tags;

        return $summary;
    }

    public function getMemberSince()
    {
        return date_create_from_format('Y-m-d H:i:s', $this->data->member_since);
    }

    public function getBanned()
    {
        if (!property_exists($this->data, 'banned')) {
            return null;
        }

        $banned = new stdClass();
        $banned->reason = $this->data->banned->reason;
        $banned->expired = $this->data->banned->expired; // @TODO: maybe it should be a DateTime?

        return $banned;
    }

}
