<?php

namespace WykopApiClient;

use stdClass;

class Profiles
{
    /**
     * @var WykopApi $apiClient
     */
    private $apiClient = null;

    /**
     * Profile constructor.
     *
     * @param WykopApi  $apiClient
     */
    public function __construct($apiClient) {
        $this->apiClient = $apiClient;
    }

    /**
     * Retrieves and returns user profile.
     *
     * @param string    $username  An username that you want to retrieve data for. If empty, logged user data will be returned.
     *
     * @return stdClass
     *
     * @throws Error
     */
    public function get($username = null) {
        $profile = $this->apiClient->request(
            empty($username) ? 'profile' : 'profile/users/' . $username
        );

        return new Profile($profile->data);
    }

    /**
     * Retrieves and returns user's entries.
     *
     * @param string    $username    An username that you want to retrieve data for.
     * @param int|null  $page        One-based page number.
     *
     * @return stdClass
     *
     * @throws Error
     */
    public function getEntries($username, mixed $page = null) {
        if (empty($username)) {
            throw new WykopApiError('Username parameter for Profile::getEntries() is required!');
        }

        if (!empty($page) && !is_numeric($page)) {
            throw new WykopApiError('Page parameter for Profile::getEntries() should be a numeric value!');
        }

        $entriest = $this->apiClient->request(
            'profile/users/' . $username . '/entries/added?page=' . $page
        );

        $entriesList = new EntriesList($entriest?->data ?? []);
        if ($entriest->data) {
            $entriesList->setPagination($entriest?->pagination ?? [], $page);
        }

        return $entriesList;
    }
}
