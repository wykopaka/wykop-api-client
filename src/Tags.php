<?php

namespace WykopApiClient;

use stdClass;

class Tags
{
    /**
     * @var WykopApi $apiClient
     */
    private $apiClient = null;

    /**
     * Tags constructor.
     *
     * @param WykopApi  $apiClient
     *
     * @return EntriesList
     */
    public function __construct($apiClient) {
        $this->apiClient = $apiClient;
    }

    /**
     * @param string $tag
     * @param mixed|null $page
     * @return EntriesList
     */
    public function getEntries(string $tag, mixed $page = null) : EntriesList
    {
        if (empty($tag)) {
            throw new WykopApiError('Tag parameter for Tags::getEntries() is required!');
        }

        $entriest = $this->apiClient->request(
            'tags/' . $tag . '/stream?sort=all&type=entry' . ($page ? '&page=' . $page : '')
        );

        $entriesList = new EntriesList($entriest->data);
        $entriesList->setPagination($entriest->pagination, $page);

        return $entriesList;
    }
}
