<?php

namespace WykopApiClient;

use stdClass;

class User
{
    /**
     * @var WykopApi $apiClient
     */
    private $apiClient = null;

    /**
     * User constructor.
     *
     * @param WykopApi $apiClient
     */
    public function __construct($apiClient) {
        $this->apiClient = $apiClient;
    }

    public function login($username, $password) {
        if (empty($username)) {
            throw new WykopApiError('Username parameter for User::login() is required!');
        }

        if (empty($password)) {
            throw new WykopApiError('Password parameter for User::login() is required!');
        }

        $request = $this->apiClient->request('login', '{
            "data": {
                "username": "' . $username . '",
                "password": "' . $password . '"
            }
        }');

        return $request;
    }
}
