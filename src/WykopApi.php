<?php

namespace WykopApiClient;

use Error;
use stdClass;

class WykopApi
{
    /**
     * @var string $apiUrl Wykop APIv3 address.
     */
    private $apiUrl = 'https://wyk.pl/api/v3';

    /**
     * @var string $appKey Application's key.
     */
    protected $appKey = null;

    /**
     * @var string  $appSecret Application's secret.
     */
    protected $appSecret = null;

    /**
     * @var string $userKey User's key.
     */
    private $userKey = null;

    /**
     * @var string $authJWT User's key.
     */
    protected $authJWT = null;

    /**
     * @var string $apiClientUseragent Useragent string used in requests to API.
     */
    private $apiClientUseragent = 'WykopAPI Client';

    public $wykopConnect = null;
    public $user = null;
    public $profile = null;
    public $privateMessage = null;

    /**
     * WykopApi constructor.
     *
     * @param string|null $customApiURL
     */
    public function __construct(?string $customApiURL = null) {
        if ($customApiURL) {
            $this->apiUrl = $customApiURL;
        }

        $this->wykopConnect = new WykopConnect($this);
        $this->user = new User($this);
        $this->profile = new Profiles($this);
        $this->tags = new Tags($this);
        $this->entries = new Entries($this);
        $this->privateMessage = new PrivateMessages($this);

        return $this;
    }

    public function authorizeApp($appKey, $appSecret) {
        if (empty($appKey)) {
            throw new WykopApiError('Application\'s key for WykopApi::_construct() is required!');
        }

        if (empty($appSecret)) {
            throw new WykopApiError('Application\'s secret for WykopApi::_construct() is required!');
        }

        $this->appKey = $appKey;
        $this->appSecret = $appSecret;

        $authRequest = $this->request(
            'auth',
            '{"data": {"key": "' . $appKey . '", "secret": "' . $appSecret . '"}}'
        );

        $this->authorize($authRequest->data->token);

        return $authRequest->data->token;
    }


    public function authorize($jwt) {
        if (empty($jwt)) {
            throw new WykopApiError('JWT for WykopAPI::authorize() is required!');
        }

        $this->authJWT = $jwt;

        return $this;
    }

    /**
     * Returns API URL.
     *
     * @return string
     */
    public function getApiUrl() {
        return $this->apiUrl;
    }

    /**
     * Returns app's key.
     *
     * @return string
     */
    public function getAppKey() {
        return $this->appKey;
    }

    /**
     * Returns app's secret.
     *
     * @return string
     */
    public function getAppSecret() {
        return $this->appSecret;
    }

    /**
     * Returns user's key.
     *
     * @return string
     */
    public function getUserKey() {
        return $this->userKey;
    }

    /**
     * Removes user's authorization.
     * Works like a logout, but keeps old key valid.
     *
     * @return $this
     */
    public function removeAuthorization() {
        $this->userKey = null;

        return $this;
    }

    /**
     * Makes request to the Wykop API.
     *
     * @param string $path      A path to the API that you want to call.
     * @param null   $postData  Data that should be send in a POST request.
     *
     * @return stdClass
     *
     * @throws Error
     */
    public function request($path, $postData = null) {
        $path = $this->apiUrl . '/' . $path;

        $requestHeaders = [
            'Content-Type: application/json',
            'Accept: application/json'
        ];

        if ($this->authJWT) {
            array_push($requestHeaders, 'Authorization: Bearer ' . $this->authJWT);
        }

        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_ENCODING => '',
            CURLOPT_USERAGENT => $this->apiClientUseragent,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HTTPHEADER => $requestHeaders,
            CURLOPT_SSLVERSION => 1
        );

        if ($postData !== null) {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $postData;
        }

        $ch  = curl_init($path);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $errorMessage = curl_error($ch);
        curl_close($ch);

        $responseContent = json_decode($content);

        if (!empty($responseContent->error)) {
            throw new WykopApiError('Request to the Wykop API failed due to following error: ' . $responseContent->error->message);
        }

        if (!empty($errorMessage)) {
            throw new WykopApiError('Request to the Wykop API failed due to following error: ' . $errorMessage);
        }

        return $responseContent;
    }
}
