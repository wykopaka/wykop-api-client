<?php

namespace WykopApiClient;

class WykopConnect
{
    /**
     * @var WykopApi $apiClient
     */
    private $apiClient = null;

    /**
     * User constructor.
     *
     * @param WykopApi $apiClient
     */
    public function __construct($apiClient) {
        $this->apiClient = $apiClient;
    }


    /**
     * Returns an URL to Wykop Connect.
     *
     * @return string
     */
    public function getUrl() {
        return $this->apiClient->request('connect')->data->connect_url;
    }
}
